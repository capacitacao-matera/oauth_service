package com.capacitacaomatera.integration;

import com.capacitacaomatera.entity.Funcionario;
import com.capacitacaomatera.repository.FuncionarioRepository;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class OauthTests {

    @MockBean
    FuncionarioRepository funcionarioRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

	@Test
	public void authorizationSuccessTest() throws Exception {
        Mockito
            .when(funcionarioRepository.findByUsuario("teste"))
            .thenReturn(
                new Funcionario.Builder()
                    .withUsuario("teste")
                    .withSenha(passwordEncoder.encode("teste"))
                    .build()
            );

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", "teste");
        params.add("password", "teste");

        String base64ClientCredentials = new String(Base64.encodeBase64("public:".getBytes()));

        ResultActions result
            = mockMvc.perform(post("/oauth/token")
            .params(params)
            .header("Authorization","Basic " + base64ClientCredentials)
            .accept("application/json;charset=UTF-8"))
            .andExpect(status().isOk());
	}

	@Test
    public void authorizationUserUnauthorizedTest() throws Exception {
        Mockito
            .when(funcionarioRepository.findByUsuario("teste"))
            .thenReturn(
                new Funcionario.Builder()
                    .withUsuario("teste")
                    .withSenha(passwordEncoder.encode("teste"))
                    .build()
            );

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", "teste");
        params.add("password", "123");

        String base64ClientCredentials = new String(Base64.encodeBase64("public:".getBytes()));

        ResultActions result
            = mockMvc.perform(post("/oauth/token")
            .params(params)
            .header("Authorization","Basic " + base64ClientCredentials)
            .accept("application/json;charset=UTF-8"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void authorizationClientUnauthorizedTest() throws Exception {
        Mockito
            .when(funcionarioRepository.findByUsuario("teste"))
            .thenReturn(
                new Funcionario.Builder()
                    .withUsuario("teste")
                    .withSenha(passwordEncoder.encode("teste"))
                    .build()
            );

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", "teste");
        params.add("password", "teste");

        String base64ClientCredentials = new String(Base64.encodeBase64("client:password".getBytes()));

        ResultActions result
            = mockMvc.perform(post("/oauth/token")
            .params(params)
            .header("Authorization","Basic " + base64ClientCredentials)
            .accept("application/json;charset=UTF-8"))
            .andExpect(status().isUnauthorized());
    }
}
