package com.capacitacaomatera.controller;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @GetMapping("/acessar")
    public ModelAndView login() {
        return new ModelAndView("login");
    }

}
