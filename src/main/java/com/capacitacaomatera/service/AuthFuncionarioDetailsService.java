package com.capacitacaomatera.service;

import com.capacitacaomatera.entity.Funcionario;
import com.capacitacaomatera.identity.FuncionarioPrincipal;
import com.capacitacaomatera.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthFuncionarioDetailsService implements UserDetailsService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Override
    public UserDetails loadUserByUsername(String s) {
        Funcionario f = funcionarioRepository.findByUsuario(s);

        if(f == null)
            throw new UsernameNotFoundException(s);

        return new FuncionarioPrincipal(f);
    }
}
