package com.capacitacaomatera.identity;

import com.capacitacaomatera.entity.Funcionario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FuncionarioPrincipal implements UserDetails {

    private Funcionario funcionario;

    public FuncionarioPrincipal (Funcionario f){
        this.funcionario = f;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("USER"));
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.funcionario.getSenha();
    }

    @Override
    public String getUsername() {
        return this.funcionario.getUsuario();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !this.funcionario.isDeleted();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !this.funcionario.isDeleted();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !this.funcionario.isDeleted();
    }

    @Override
    public boolean isEnabled() {
        return !this.funcionario.isDeleted();
    }
}
