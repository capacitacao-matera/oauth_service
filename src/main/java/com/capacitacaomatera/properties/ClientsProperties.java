package com.capacitacaomatera.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ConfigurationProperties("auth-client")
public class ClientsProperties {

    private String clientId = "public";
    private String clientSecret = "";
    private boolean autoApprove = true;
    private int accessTokenValiditySeconds = 36000;
    private int refreshTokenValiditySeconds = 36000;
    private List<String> grantTypes = new ArrayList<>(Arrays.asList("password", "authorization_code", "refresh_token"));
    private List<String> scopes = new ArrayList<>(Arrays.asList("read", "write"));
    private List<String> redirectUris = new ArrayList<>(Arrays.asList("http://localhost/"));

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setAutoApprove(boolean autoApprove) {
        this.autoApprove = autoApprove;
    }

    public void setAccessTokenValiditySeconds(int accessTokenValiditySeconds) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    public void setRefreshTokenValiditySeconds(int refreshTokenValiditySeconds) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }

    public void setGrantTypes(List<String> grantTypes) {
        this.grantTypes = grantTypes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    public void setRedirectUris(List<String> redirectUris) {
        this.redirectUris = redirectUris;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public boolean isAutoApprove() {
        return autoApprove;
    }

    public int getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    public int getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    public List<String> getGrantTypes() {
        return grantTypes;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public List<String> getRedirectUris() {
        return redirectUris;
    }
}
