package com.capacitacaomatera.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Where(clause = "deleted = 0")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Funcionario implements Serializable {

    @Id
    private Long id;

    @NotNull
    @Size(max = 6)
    private String usuario;

    @NotNull
    private String senha;

    @NotNull
    private String updatedAt;

    @NotNull
    private boolean deleted;

    public Funcionario() {/*Hibernate*/}

    public String getUsuario() {
        return usuario;
    }

    public String getSenha() {
        return senha;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public static final class Builder {

        private String usuario;
        private String senha;

        public Builder withUsuario(String u) {
            this.usuario = u;
            return this;
        }

        public Builder withSenha(String s) {
            this.senha = s;
            return this;
        }

        public Funcionario build() {
            Funcionario f = new Funcionario();

            f.usuario = usuario;
            f.senha = senha;

            return f;
        }
    }
}
