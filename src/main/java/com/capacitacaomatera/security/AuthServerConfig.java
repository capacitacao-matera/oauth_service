package com.capacitacaomatera.security;

import com.capacitacaomatera.properties.ClientsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;
import java.util.List;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig  extends AuthorizationServerConfigurerAdapter {

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private TokenStore tokenStore;

    @Autowired
    private ClientsProperties clientProperties;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.passwordEncoder(passwordEncoder)
            .tokenKeyAccess("permitAll()")
            .checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        List<String> grants = clientProperties.getGrantTypes();
        List<String> scopes = clientProperties.getScopes();
        List<String> uris = clientProperties.getRedirectUris();

        clients
            .inMemory()
            .withClient(clientProperties.getClientId())
            .secret(passwordEncoder.encode(clientProperties.getClientSecret()))
            .authorizedGrantTypes(grants.toArray(new String[grants.size()]))
            .scopes(scopes.toArray(new String[scopes.size()]))
            .autoApprove(clientProperties.isAutoApprove())
            .accessTokenValiditySeconds(clientProperties.getAccessTokenValiditySeconds())
            .refreshTokenValiditySeconds(clientProperties.getRefreshTokenValiditySeconds())
            .redirectUris(uris.toArray(new String[uris.size()]))
            .and()
            .build();
    }

    @Override
    public void configure (AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
            .authenticationManager(authenticationManager)
            .tokenStore(tokenStore);
    }

    @Bean
    public ClientsProperties clientProperties() {
        return new ClientsProperties();
    }

    @Bean
    public TokenStore tokenStore(DataSource dataSource) {
        return new JdbcTokenStore(dataSource);
    }

}
